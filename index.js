"use strict";

// Теоретичні питання
// 1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// 2.Які засоби оголошення функцій ви знаєте?
// 3.Що таке hoisting, як він працює для змінних та функцій?

// 1. Екранування це процес додавання спеціальних символів перед певними знаками або послідовностями символів у рядках
// 2. оголошення функцій бувають
//   а) Оголошення функції з використанням ключового слова function
//   б) Оголошення функції як виразу з використанням змінної
//   в) Стрілкова функція
//   г) Оголошення функції як методу обєкта
// 3. hoisting означає, що декларації змінних та функцій переносяться вгору своєї області видимості перед виконанням коду, це означає що ви можете використовувати змінні та функції до того, як вони були декларовані у коді.

let newUser = {};

let createNewUser = () => {
  newUser = {
    _firstName: prompt("Введіть ім'я:"),
    _lastName: prompt("Введіть прізвище:"),
    birthday: prompt("Введіть дату народження:"),

    get firstName() {
      return this._firstName;
    },

    get lastName() {
      return this._lastName;
    },

    setFirstName: function (newFirstName) {
      this._firstName = newFirstName;
    },
    setLastName: function (newLastName) {
      this._lastName = newLastName;
    },
    getLogin: function () {
      return `${this._firstName[0]}${this._lastName}`.toLowerCase();
    },
    getAge: function () {
      const [day, month, year] = this.birthday.split(".");
      const dob = new Date(year, month - 1, day);
      const today = new Date();
      const diffMs = today - dob;
      const age = Math.floor(diffMs / 31557600000); // 31557600000 - мілісекунд у році

      return age;
    },
    getPassword: function () {
      const [day, month, year] = this.birthday.split(".");
      const firstNameInitial = this._firstName[0].toUpperCase();
      const lastName = this._lastName.toLowerCase();
      const birthYear = year;

      return firstNameInitial + lastName + birthYear;
    },
  };
  return newUser;
};

Object.defineProperty(newUser, "_firstName", {
  writable: false,
});
Object.defineProperty(newUser, "_lastName", {
  writable: false,
});

console.log(createNewUser());
console.log(newUser.getAge());
console.log(newUser.getPassword());
